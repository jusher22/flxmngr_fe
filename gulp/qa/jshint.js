"use strict";

var gulp = require( "gulp" );
var jshint = require( "gulp-jshint" );
var routes = require( "../routes" );
var gutil = require( "gulp-util" );
var stylish = require( "jshint-stylish" );

module.exports = jsHint;

//.pipe( jshint.reporter( "fail" ) )
function jsHint() {
  return gulp.src( routes.scripts.watch )
    .pipe( jshint() )
    .pipe( jshint.reporter( stylish ) )
    .on( "error", gutil.log );
}
