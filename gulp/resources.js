"use strict";
var gulp = require( "gulp" );
var routes = require( "./routes" );
var gutil = require( "gulp-util" );
var plumber = require( "gulp-plumber" );
var notify = require( "gulp-notify" );

exports.resources = resources;
exports.getFonts = getFonts;

function resources() {
  return gulp.src( routes.resources.main, {
      base: "./src"
    } )
    .pipe( plumber( {
      errorHandler: notify.onError( "Error: <%= error.message %>" )
    } ) )
    .pipe( gulp.dest( "dist/" ) )
    .on( "error", gutil.log );
}

function getFonts() {
  return gulp.src( "bower_components/font-awesome/fonts/**/*.{eot,svg,ttf,woff,woff2}" )
    .pipe( gulp.dest( "dist/resources/fonts/font-awesome" ) )
    .on( "error", gutil.log );
}
