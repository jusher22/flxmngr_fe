"use strict";

var gulp = require( "gulp" );
var rimraf = require( "rimraf" );
require( "./qa/tasks.js" );

gulp.task( "clean", clean );
gulp.task( "resources", require( "./resources" ).resources );
gulp.task( 'fonts', require( "./resources" ).getFonts );

gulp.task( "build:js", require( "./browserify" ).development );
gulp.task( "build:bower", require( "./vendor" ).development );
gulp.task( "build:html", require( "./templates" ).development );
gulp.task( "build:sass", require( "./sass" ).development );
gulp.task( "build", [ "build:js", "build:bower", "build:html",
 "build:sass", "resources", "fonts" ], function( cb ) {
   return cb();
 } );

gulp.task( "dist:js", require( "./browserify" ).production );
gulp.task( "dist:bower", require( "./vendor" ).production );
gulp.task( "dist:html", require( "./templates" ).production );
gulp.task( "dist:sass", require( "./sass" ).production );
gulp.task( "dist", [ "dist:js", "dist:bower", "dist:html",
 "dist:sass", "resources", "fonts" ], function( cb ) {
   return cb();
 } );

gulp.task( "watch:js", require( "./browserify" ).watch );
gulp.task( "watch", [ "watch:js" ], require( "./watch" ) );

gulp.task( "serve:dev", [ "clean", "build", "watch" ], require( "./server" ) );
gulp.task( "serve:dist", [ "clean", "dist" ], require( "./server" ) );

gulp.task( "serve", require( "./server" ) );
gulp.task( "default", [ "serve:dev" ] );

function clean( callback ) {
  rimraf.sync( "./dist" );
  return callback();
}
