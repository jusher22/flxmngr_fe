"use strict";

module.exports = {
  sass: {
    main: "./src/sass/main.scss",
    watch: [ "src/sass/*.scss", "src/sass/**/**/*.scss", "src/app/*.scss",
      "src/app/**/*.scss", "src/app/sass/**/*.scss"
    ]
  },
  scripts: {
    base: "src/app/",
    main: "./src/app/index.js",
    watch: [ "src/app/*.js", "src/app/**/*.js" ]
  },
  templates: {
    watch: [ "src/app/*.html", "src/app/**/*.html",
      "src/app/**/htmls/*.html", "src/app/**/views/*.html"
    ]
  },
  resources: {
    main: [ "src/index.html", "src/resources", "src/resources/*",
      "src/resources/**/*", "src/resources/js/*.js"
    ]
  }
};
