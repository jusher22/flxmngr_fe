"use strict";

var _name = require( "../../../package.json" ).name + ".login";

var mod = angular.module( _name, [] );
mod.config( require( "./routes" ) );

mod.service( "LoginApi", require( "./services/loginApi" ) );
mod.controller( "LoginCtrl", require( "./controllers/loginCtrl" ) );

module.exports = mod;
