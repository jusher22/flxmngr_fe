"use strict";

module.exports = LoginApi;

LoginApi.$inject = [ "$http", "urlApi" ];

function LoginApi( $http, urlApi ) {
  LoginApi.login = login;
  return LoginApi;

  function login( user ) {
    return $http.post( urlApi + "/authenticate/login", user )
      .then( response, response );
  }

  function response( resp ) {
    return resp;
  }
}
