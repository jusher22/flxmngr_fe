"use strict";

module.exports = loginCtrl;

loginCtrl.$inject = [
  "LoginApi",
  "$sessionStorage",
  "$localStorage",
  "$http",
  "$state",
  "Notifications"
];

function loginCtrl( LoginApi, $sessionStorage, $localStorage, $http, $state, Notifications ) {
  var vm = this;
  vm.user = {
    remember: true
  };
  vm.login = login;

  function login( user, form ) {
    if ( form.$valid ) {
      LoginApi.login( user ).then( function ( resp ) {
        if ( resp.status === 200 ) {
          $sessionStorage.currentUser = resp.data;

          //if user selects to be remembered in the browser
          if ( user.remember ) {
            $localStorage.currentUser = resp.data;
          }

          //sets the Authorization Header for restAPI requests
          $http.defaults.headers.common.Authorization = "Bearer " + resp.data.token;
          $state.go( "index.dashboard" );
          return false;
        }
        vm.errorMessage = getErrorMessage( resp.status );
      } );
    }
  }

  function getErrorMessage( status ) {
    var statuses = {
      404: "Incorrect user name",
      401: "Incorrect password"
    };
    return statuses[ status ] + ". Pleae try again";
  }

}
