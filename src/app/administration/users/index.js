"use strict";

var _name = require("../../../../package.json").name + ".users";

var mod = angular.module( _name, [] );
mod.service("UserApi", require("./services/userApi"));
mod.controller("UsersListCtrl", require("./controllers/usersListCtrl"));
mod.controller("UserDetailCtrl", require("./controllers/userDetailCtrl"));
module.exports = mod;
