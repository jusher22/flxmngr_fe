"use strict";

module.exports = UserApi;

UserApi.$inject = [ "$http", "urlApi" ];

function UserApi( $http, urlApi ) {
  UserApi.listUsers = listUsers;
  UserApi.getUser = getUser;
  UserApi.saveUser = saveUser;
  UserApi.updateUser = updateUser;
  return UserApi;

  function listUsers() {
    return $http.get( urlApi + "/users" ).then( success );
  }

  function getUser( id ) {
    return $http.get( urlApi + "/users/" + id ).then( success );
  }

  function saveUser( user ) {
    return $http.post( urlApi + "/users", user ).then( success );
  }

  function updateUser( user ) {
    return $http.put( urlApi + "/users/" + user._id, user ).then( success );
  }

  function success( resp ) {
    return resp;
  }
}
