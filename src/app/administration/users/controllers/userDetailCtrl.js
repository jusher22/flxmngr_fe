"use strict";

module.exports = UserDetailCtrl;

UserDetailCtrl.$inject = [ "user", "UserApi", "Notifications", "$state" ];

function UserDetailCtrl( user, UserApi, Notifications, $state ) {
  var vm = this;
  vm.section = $state.current.data;

  vm.saveUser = saveUser;
  vm.goBack = goBack;


  ( function init() {
    if ( !user.newUser ) {
      vm.user = user;
    }
  } )();

  function saveUser( form, vmUser ) {
    if ( form.$valid && !form.$pristine ) {
      if ( user.newUser ) {
        create( vmUser );
      } else {
        update( vmUser );
      }
    }
  }

  function create( user ) {
    user.password = "abc1234";
    UserApi.saveUser( user )
      .then( function ( resp ) {
        if ( resp.status === 201 ) {
          user._id = resp.data;
          Notifications.add( resp.status, "User" );
        }
      } )
      .finally( function () {
        //vm.user = user;
        console.log( vm.user );
      } );
  }

  function update( user ) {
    UserApi.updateUser( user )
      .then( function ( resp ) {
        if ( resp.status === 200 ) {
          user._id = resp.data;
          Notifications.add( resp.status );
        }
      } );
  }

  function goBack() {
    window.history.back();
  }
}
