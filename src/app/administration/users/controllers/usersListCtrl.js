"use strict";

module.exports = usersListCtrl;

usersListCtrl.$inject = [ "list", "$state", "UserApi" ];

function usersListCtrl( list, $state, UserApi ) {
  var vm = this;
  vm.list = list;
  vm.section = $state.current.data;

  vm.enableUser = enableUser;

  function enableUser( event, user ) {
    event.stopPropagation();
    user.enabled = !user.enabled;
    UserApi.updateUser( user );
  }
}
