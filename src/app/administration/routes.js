"use strict";

module.exports = routes;

routes.$inject = [ "$stateProvider" ];

function routes( $stateProvider ) {
  $stateProvider.state( "index.administration", {
    templateUrl: "administration/views/template.html",
    url: "/administration",
    abstract: true
  } );

  /***************************************************************************/

  $stateProvider.state( "index.administration.users", {
    url: "/users",
    templateUrl: "administration/users/views/usersList.html",
    resolve: {
      list: getUsers
    },
    data:{
      section: "Administration",
      subsection: "Users"
    },
    controller: "UsersListCtrl",
    controllerAs: "vm"
  } );

  getUsers.$inject = [ "UserApi", "Notifications" ];

  function getUsers( UserApi, Notifications ) {
    return UserApi.listUsers()
      .then( function ( resp ) {
        return resp.data;
      }, function ( err ) {
        Notifications.add( err.status, "Users List" );
        return [];
      } );
  }

  /***************************************************************************/

  $stateProvider.state( "index.administration.userDetail", {
    url: "administration/user/:id",
    templateUrl: "administration/users/views/userDetail.html",
    resolve: {
      user: getUser
    },
    data:{
      section: "Administration",
      subsection: "User Detail"
    },
    controller: "UserDetailCtrl",
    controllerAs: "vm"
  } );


  getUser.$inject = [ "$stateParams", "UserApi", "Notifications" ];

  function getUser( $stateParams, UserApi, Notifications ) {
    if ( $stateParams.id ) {
      return UserApi.getUser( $stateParams.id )
        .then( function ( resp ) {
          return resp.data;
        }, function ( err ) {
          Notifications.add( err.status, "User" );
          return {};
        } );
    } else {
      return {
        newUser: true
      };
    }
  }

  /***************************************************************************/
}
