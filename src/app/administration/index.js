"use strict";

var _name = require( "../../../package.json" ).name + ".administration";

var mod = angular.module( _name, [
  require( "./users" ).name
]);
mod.config( require("./routes") );

module.exports = mod;
