"use strict";

var _name = require( "../../../package.json" ).name + ".tools";

var mod = angular.module( _name, [
  require( "./estimation-tool" ).name,
  require( "./flex" ).name
] );

mod.config( require( "./routes" ) );

module.exports = mod;
