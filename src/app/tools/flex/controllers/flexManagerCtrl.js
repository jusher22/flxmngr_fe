"use strict";

module.exports = flexManagerCtrl;

flexManagerCtrl.$inject = [ "$state" ];

function flexManagerCtrl( $state ) {
  var vm = this;
  vm.menu = $state.current.data.menu;
}
