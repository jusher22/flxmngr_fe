"use strict";

var _name = require( "../../../../package.json" ).name + ".flexManager";

var mod = angular.module( _name, [] );
mod.controller( "FlexManagerCtrl", require( "./controllers/flexManagerCtrl" ) );

module.exports = mod;
