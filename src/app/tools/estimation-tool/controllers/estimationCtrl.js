"use strict";

module.exports = homeCtrl;

homeCtrl.$inject = [ "$localStorage", "$scope" ];

function homeCtrl( $localStorage, $scope ) {
  var vm = this;
  vm.project = $localStorage.project;
  vm.goBack = goBack;

  $scope.$watchCollection( "vm.estimation", function () {
    if ( vm.project ) {
      var estimation = estimate( vm.estimation );
      vm.total = estimation.total;
      vm.models = estimation.modelProps;
    }
  } );

  function estimate( model ) {
    var total = 0;
    var modelProps = [];
    for ( var prop in model ) {
      if ( model.hasOwnProperty( prop ) ) {
        total += parseInt( model[ prop ] );
        modelProps.push( prop );
      }
    }
    return {
      total: total,
      modelProps: modelProps
    };
  }

  function goBack() {
    window.history.back();
  }

}
