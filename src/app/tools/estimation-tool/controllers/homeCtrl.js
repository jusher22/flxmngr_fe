"use strict";

module.exports = EstimationCtrl;
EstimationCtrl.$inject = [ "$scope", "DataApi", "$state", "$localStorage" ];

function EstimationCtrl( $scope, DataApi, $state, $localStorage ) {

  var vm = this;

  //view values
  vm.total = 0;
  vm.section = $state.current.data;
  vm.selectedProject = false;
  vm.projects = _.map( DataApi, function ( project ) {
    project.modelName = project.name.split( " " ).join( "" ).toLowerCase();
    return project;
  } );

  //view methods declaration
  vm.goToProject = goToProject;


  //view methods implementation
  function goToProject( project ) {
    $localStorage.project = project;
    $state.go( "index.tools.newEstimation", {
      projectType: project.modelName
    } );
  }

}
