"use strict";

var _name = require("../../../../package.json").name + ".estimationTool";

var mod = angular.module( _name, [] );
mod.service( "DataApi", require( "./services/dataApi" ) );
mod.controller( "EstimationCtrl", require( "./controllers/estimationCtrl" ) );
mod.controller( "HomeCtrl", require( "./controllers/homeCtrl" ) );

module.exports = mod;
