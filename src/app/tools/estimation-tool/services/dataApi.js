"use strict";

module.exports = DataApi;

function DataApi() {
  return [ {
    name: "Web Site",
    icon: "fa fa-globe",
    questions: [ {
      model: "pages",
      type: "radio",
      question: "How many pages does the web site have?",
      answers: [ {
        label: "1-3",
        value: 24
      }, {
        label: "4-9",
        value: 72
      }, {
        label: "10+",
        value: 100
      } ]
    }, {
      model: "login",
      type: "radio",
      question: "Do users need to signup/login?",
      answers: [ {
        label: "No",
        value: 0
      }, {
        label: "User/Password",
        value: 24
      }, {
        label: "Social/Media",
        value: 28
      }, {
        label: "Both",
        value: 32
      } ]
    }, {
      model: "profile",
      type: "radio",
      question: "Will the user be able to create and manage a profile",
      answers: [ {
        label: "No",
        value: 0
      }, {
        label: "Yes",
        value: 16
      } ]
    }, {
      model: "e-commerce",
      type: "radio",
      question: "Is the site intended to be a commerce site",
      answers: [ {
        label: "No",
        value: 0
      }, {
        label: "Yes",
        value: 40
      } ]
    }, {
      model: "rate",
      type: "radio",
      question: "Do users rate or review things?",
      answers: [ {
        label: "No",
        value: 0
      }, {
        label: "Yes",
        value: 24
      } ]
    }, {
      model: "api",
      type: "radio",
      question: "Does your website need to connect with another app or website",
      answers: [ {
        label: "No",
        value: 0
      }, {
        label: "Yes",
        value: 32
      } ]
    }, {
      model: "search",
      type: "radio",
      question: "Does your website need search?",
      answers: [ {
        label: "No",
        value: 0
      }, {
        label: "Yes",
        value: 32
      } ]
    }, {
      model: "cms",
      type: "radio",
      question: "Do you need a Content Management System (CMS)?",
      answers: [ {
        label: "No",
        value: 0
      }, {
        label: "Yes",
        value: 24
      } ]
    } ]
  }, {
    name: "Banners",
    icon: "fa fa-flag",
    questions: [ {
      model: "banners",
      type: "number",
      question: "How many banners does the campaing will have?",
      answers: [ {
        label: "1-3",
        value: 24
      }, {
        label: "4-9",
        value: 72
      }, {
        label: "10+",
        value: 100
      } ]
    }, {
      model: "other",
      type: "radio",
      question: "Camping comes in different language?",
      answers: [ {
        label: "No",
        value: 0
      }, {
        label: "Yes",
        value: 24
      } ]
    } ]
  }/*, {
    name: "Web Application",
    icon: "fa fa-cogs",
    questions: []
  }, {
    name: "Movil Application",
    icon: "fa fa-mobile",
    questions: []
  }, {
    name: "Emails",
    icon: "fa fa-envelope-o",
    questions: []
  }*/ ];
}
