"use strict";

module.exports = routes;

routes.$inject = [ "$stateProvider" ];

function routes( $stateProvider ) {
  $stateProvider.state( "index.tools", {
    templateUrl: "tools/views/uiView.html",
    url: "/tools",
    abstract: true,
    data: {
      section: "Tools"
    }
  } );

  /***************************************************************************/

  $stateProvider.state( "index.tools.estimationTool", {
    url: "/estimation-tool",
    templateUrl: "tools/estimation-tool/views/home.html",
    data: {
      subsection: "Estimation Tool"
    },
    controller: "HomeCtrl",
    controllerAs: "vm"
  } );

  /***************************************************************************/

  $stateProvider.state( "index.tools.newEstimation", {
    url: "/estimation-tool/:projectType",
    templateUrl: "tools/estimation-tool/views/project.html",
    controller: "EstimationCtrl",
    controllerAs: "vm"
  } );

  /***************************************************************************/

  $stateProvider.state( "index.tools.flexMngr", {
    templateUrl: "tools/flex/views/uiView.html",
    url: "/flex-manager",
    data: {
      subsection: "Flex Manager",
      menu: [ {
          state: "index.tools.flexMngr.calendar",
          tag: "Calendar"
        },
        {
          state: "index.tools.flexMngr.tracker",
          tag: "Tracker"
        },
        {
          state: "index.tools.flexMngr.resources",
          tag: "Resources"
        }
      ]
    },
    controller: "FlexManagerCtrl",
    controllerAs: "vm"
  } );

  $stateProvider.state( "index.tools.flexMngr.calendar", {
    url: "/calendar",
    template: "<h3>Calendar goes here</h3>"
  } );

  $stateProvider.state( "index.tools.flexMngr.tracker", {
    url: "/hour-track",
    template: "<h3>Track goes here</h3>"
  } );

  $stateProvider.state( "index.tools.flexMngr.resources", {
    url: "/resources",
    template: "<h3>Resources go here</h3>"
  } );


}
