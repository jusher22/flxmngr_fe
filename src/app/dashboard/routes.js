"use strict";

module.exports = routes;

routes.$inject = [ "$stateProvider" ];

function routes( $stateProvider ) {

  $stateProvider.state( "index.dashboard", {
    url: "/",
    templateUrl: "dashboard/views/dashboard.html",
  } );

}
