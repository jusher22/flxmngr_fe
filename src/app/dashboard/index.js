"use strict";

var _name = require( "../../../package.json" ).name + ".home";

var mod = angular.module( _name, [] );
mod.config( require( "./routes" ) );

module.exports = mod;
