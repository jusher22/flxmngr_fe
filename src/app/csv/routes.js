"use strict";

module.exports = routes;

routes.$inject = [ "$stateProvider" ];

function routes( $stateProvider ) {

  $stateProvider.state( "index.csv", {
    url: "/csv",
    templateUrl: "csv/views/csv.html",
    controller: "CsvCtrl",
    controllerAs: "vm"
  } );

}
