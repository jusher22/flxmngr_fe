"use strict";

module.exports = csvCtrl;

csvCtrl.$inject = [];

function csvCtrl() {
  var vm = this;
  vm.processFile = processFile;

  function processFile() {

    var file = document.getElementById( "userFile" ).files[ 0 ];
    if ( file ) {
      getAsText( file );
    }

    function getAsText( file ) {

      var reader = new FileReader();
      reader.readAsText( file, "UTF-8" );
      reader.onload = function ( evt ) {
        var fileString = evt.target.result;
        var resources = fileString.split( "\n" );
        vm.resourcesList = [];
        resources.forEach( function ( r ) {
          vm.resourcesList.push( getResource( r.match( /(".*?"|[^",\s]+)(?=\s*,|\s*$)/g ) ) );
        } );
        console.log( vm.resourcesList );
      };

    } // end getAsText

    function getResource( resourceRow ) {
      return {
        id: resourceRow[ 0 ],
        firstName: resourceRow[ 1 ],
        lastName: resourceRow[ 2 ],
        country: resourceRow[ 3 ],
        code: resourceRow[ 4 ],
        comment: resourceRow[ 5 ] || ""
      };
    }

  }
}
