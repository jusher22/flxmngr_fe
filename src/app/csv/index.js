"use strict";

var _name = require( "../../../package.json" ).name + ".csv";

var mod = angular.module( _name, [] );
mod.config( require( "./routes" ) );
mod.controller( "CsvCtrl", require( "./controllers/csvCtrl" ) );

module.exports = mod;
