"use strict";
module.exports = inViewportDirective;

inViewportDirective.$inject = ['$timeout']

function inViewportDirective($timeout) {
  return {
    restrict: "A",
    link: function (scope, element, attrs) {
      var container = element[0];

      $timeout(function() {
        if (!isElementInViewport(container)) {
          var offSetY = getOffset(container).top,
          wHeight = window.innerHeight,
          newHeight = (wHeight - offSetY);
          container.setAttribute("style", "height: calc("+newHeight+"px - 12px);overflow:auto;");
        }
      }, 500);
    }
  };

  /**
  * Returns if element is on viewport or not
  * @param {element} type - current container of the DOM element
  */
  function isElementInViewport (el) {
    var rect = el.getBoundingClientRect();
    return (
      rect.top >= 0 &&
      rect.left >= 0 &&
      rect.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
      rect.right <= (window.innerWidth || document.documentElement.clientWidth)
    );
  }

  /** NOTE: This function maybe placed in a utilities.js file, it may be requestes
  * Returns element offset / top or left
  * @param {element} type - current container of the DOM element
  */
  function getOffset(el) {
    el = el.getBoundingClientRect();
    return {
      left: el.left,
      top: el.top
    }
  }
}
