"use strict";

module.exports = {
  templateUrl: "base/components/views/sectionHeader.html",
  controller: [ "$state", function ( $state ) {
    this.section = $state.current.data;
  } ]
};
