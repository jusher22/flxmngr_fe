"use strict";

module.exports = {
  templateUrl: "base/components/views/moduleMenu.html",
  bindings: {
    menu: "<"
  }
};
