"use strict";

module.exports = menu;

function menu() {

  return [ {
    section: "Dashboard",
    icon: "fa-tachometer",
    state: "index.dashboard",
  }, {
    section: "Tools",
    icon: "fa-suitcase",
    subsections: [ {
        tag: "Estimation Tool",
        state: "index.tools.estimationTool"
      },
      {
        tag: "Flex Manger",
        state: "index.tools.flexMngr"
      }
    ]
  }, {
    section: "Administration",
    icon: "fa-cog",
    subsections: [ {
        tag: "Users",
        state: "index.administration.users"
      },
      {
        tag: "Settings",
        state: "index.settings"
      }
    ]
  } ];

} // ends menu
