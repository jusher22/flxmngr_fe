"use strict";

module.exports = Notifications;

Notifications.$inject = [ "toastr" ];

function Notifications( toastr ) {
  return {
    custom: custom,
    add: add
  };

  /**
   * Returns a custom toastr message
   * @param {string} type - success, info, error, warning
   * @param {string} title - title for the notification
   * @param {string} message - content text for the notification
   */
  function custom( type, customTitle, customMessage ) {
    switch ( type ) {
    case "success":
      toastr.success( customMessage, customTitle );
      break;
    case "info":
      toastr.info( customMessage, customTitle );
      break;
    case "error":
      toastr.error( customMessage, customTitle );
      break;
    case "warning":
      toastr.warning( customMessage, customTitle );
      break;
    default:

    }
  }

  /**
   * Returns a toastr message according to standar http status
   * @param {number} staus - http status;
   * @param {string} doc - name of the doc you're working
   */
  function add( status, doc ) {
    var elem = doc || "document";
    var successMessages = {
      200: "200 - Your request has been successfuly completed",
      201: "201 - The element " + elem + " has been successfully saved",
      204: "204 - The element " + elem + " has been successfully deleted"
    };
    var errorMessages = {
      400: "400 - Missing data required to complete the request",
      401: "401 - Invalid credentials",
      403: "403 - Insufficient privileges to access requestesd data",
      404: "404 - " + elem + " not found",
    };
    var warningMessages = {
      500: "500 - " +
        "There has been an unexpected error, please try again later",
      501: "501 - The service has not been implemented",
      530: "530 - We couldn't complete your request because" +
        " It took us too long to process, please try again later"
    };
    if ( status >= 200 && status < 300 ) {
      toastr.success( successMessages[ status ] );
    }
    if ( status >= 400 && status < 500 ) {
      toastr.error( errorMessages[ status ] );
    }
    if ( status >= 500 ) {
      toastr.warning( warningMessages[ status ] );
    }
  }

}
