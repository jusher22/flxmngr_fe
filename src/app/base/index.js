"use strict";

var _name = require( "../../../package.json" ).name + ".base";

var mod = angular.module( _name, [] );
// Config
mod.config( require( "./routes" ) );
// Factory
mod.factory( "Menu", require( "./services/menu" ) );
mod.factory( "Notifications", require( "./services/notifications" ) );
// Controllers
mod.controller( "NavigationCtrl", require( "./controllers/navigationCtrl" ) );
// Directives
mod.directive( "inViewport", require( "./directives/inViewportDirective" ) );
// Componets
mod.component( "sectionHeader", require( "./components/sectionHeader" ) );
mod.component( "moduleMenu", require( "./components/moduleMenu" ) );

module.exports = mod;
