"use strict";

module.exports = navigationCtrl;

navigationCtrl.$inject = [ "Menu", "$localStorage", "$sessionStorage", "$state" ];

function navigationCtrl( Menu, $localStorage, $sessionStorage, $state ) {
  var vm = this;
  vm.menu = Menu;
  vm.me = $sessionStorage.currentUser.user;
  vm.logout = logout;
  vm.display = display;

  function logout() {
    if ( confirm( "Are you sure you want to logout?" ) ) {
      delete $localStorage.currentUser;
      delete $sessionStorage.currentUser;
      $state.go( "login" );
    }
  }

  function display(item){
    //either the item has a state of its own
    if(item.state){
      $state.go(item.state);
    }
    //or has a submenu to display
    else{
      item.show = !item.show;
    }
  }
}
