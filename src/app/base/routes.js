"use strict";

module.exports = routes;
routes.$inject = [ "$stateProvider", "$urlRouterProvider" ];

function routes( $stateProvider, $urlRouterProvider ) {

  $stateProvider.state( "index", {
    templateUrl: "base/views/template.html",
    abstract: true
  } );

  $stateProvider.state( "404", {
    url: "/404",
    templateUrl: "base/views/404.html",
  } );

  $urlRouterProvider.otherwise( "/" );
} //routes
