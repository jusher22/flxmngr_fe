"use strict";

var _name = require( "../../package.json" ).name;

var mod = angular.module( _name, [
  "ngAnimate",
  "ngAria",
  "ngTouch",
  "ngSanitize",
  "ngStorage",
  "toastr",
  "ui.router",
  require( "./base" ).name,
  require( "./login" ).name,
  require( "./administration" ).name,
  require( "./tools" ).name,
  require( "./dashboard" ).name
  //require( "./estimation-tool" ).name,
  //require( "./csv/" ).name
] );

mod.constant( "urlApi", "http://localhost:3001/api" );

mod.config( config );
mod.run( run );

config.$inject = [
  "$compileProvider",
  "$locationProvider",
  "$localStorageProvider",
  "$sessionStorageProvider",
  "$qProvider",
  "toastrConfig"
];

function config( $compileProvider, $locationProvider, $localStorageProvider,
  $sessionStorageProvider, $qProvider, toastrConfig ) {

  // By default Angular uses # in it's URLS.
  // When set to true html5Mode, # is removed from our URLS
  // http://diveintohtml5.info/history.htm for more information
  html5Mode( true );
  $qProvider.errorOnUnhandledRejections( false );
  $compileProvider.debugInfoEnabled( false );

  function html5Mode( mode ) {
    if ( window.history && window.history.pushState ) {
      $locationProvider.html5Mode( {
        enabled: mode,
        requireBase: true
      } );
    }
  } //html5Mode

  //sets storage prefix
  $localStorageProvider.setKeyPrefix( "prdgs " );
  $sessionStorageProvider.setKeyPrefix( "prdgs " );
  configureToastr( toastrConfig );

}

function configureToastr( toastrConfig ) {
  angular.extend( toastrConfig, {
    autoDismiss: true,
    containerId: "toast-container",
    maxOpened: 0,
    newestOnTop: true,
    positionClass: "toast-top-right",
    preventDuplicates: false,
    preventOpenDuplicates: false,
    target: "body",
    closeButton: true,
    closeHtml: "<i class='fa fa-fw fa-times'></i>",
    progressBar: true,
    timeOut: 7000
  } );
}

run.$inject = [
  "$rootScope",
  "$state",
  "$sessionStorage",
  "$localStorage",
  "$http"
];

function run( $rootScope, $state, $sessionStorage, $localStorage, $http ) {
  $rootScope.$on( "$stateChangeStart", function ( event, toState ) {
    //if user is authorized skip longin screen and go to main page
    if ( isAuthorized( $sessionStorage ) ) {
      if ( toState.name === "login" ) {
        event.preventDefault();
        $state.go( "index.dashboard" );
      }
      $http.defaults.headers.common.Authorization = "Bearer " + $sessionStorage.currentUser.token;
    } else {
      if ( $localStorage.currentUser ) {
        $sessionStorage.currentUser = $localStorage.currentUser;
        $http.defaults.headers.common.Authorization = "Bearer " + $localStorage.currentUser.token;
        event.preventDefault();
        $state.go( "index.dashboard" );
      } else {
        if ( !toState.free ) {
          event.preventDefault();
          $state.go( "login" );
        }
      }
    }

    function isAuthorized( $sessionStorage ) {
      if ( $sessionStorage.currentUser ) {
        return !!$sessionStorage.currentUser.token;
      }
    }

  } );

}

module.exports = mod;

angular.element( document )
  .ready( function () {
    angular.bootstrap( document, [ mod.name ], {
      strictDi: true
    } );
  } );
