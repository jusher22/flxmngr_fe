( function ( $ ) {
  "use strict";

  $( document ).ready( function () {

    /**** Tabs controller *****/
    //Init display for all components

    //Find all tabs Components
    var tabsGroupIds = [];
    _.forEach( $( ".tabs" ), function ( element ) {
      tabsGroupIds.push( $( element ).attr( "id" ) );
    } );

    //activate first tab of component
    _.forEach( tabsGroupIds, function ( id ) {
      $( "#" + id + " .tabs__group-item a" )[ 0 ].className += "active";
      var tabs__content = $( "#" + id + " .tabs__content" );
      setContentToDisplay( tabs__content );
    } );

    function setContentToDisplay( items ) {
      _.forEach( items, function ( item ) {
        item.style.display = "none";
      } );
      items[ 0 ].style.display = "block";
    }

    //Action on click for each tab
    var tabs = $( ".tabs__group-item > a" );
    tabs.click( function () {
      var id = $( this ).parent().parent().parent().attr( "id" );
      var tabs__content = $( "#" + id + " .tabs__content" );
      var thisTab = $( "#" + id + " .tabs__group-item > a" );
      activateTab( thisTab, this, $( this ).attr( "href" ), tabs__content );
    } );

    function activateTab( tabs, selected, id, contents ) {
      _.forEach( tabs, function ( tab ) {
        tab.classList.remove( "active" );
      } );
      selected.className += "active";
      _.forEach( contents, function ( content ) {
        content.style.display = "none";
      } );
      $( id ).css( "display", "block" );
    }

    /**** Accordion controller *****/
    _.forEach( $( ".accordion__content" ), function ( content ) {
      $( content ).css( {
        "max-height": "0",
        "padding": "0 10px",
        "opacity": "0"
      } );
    } );
    $( ".accordion__header" ).click( function () {
      var icon = $( this ).children( ".fa" );
      var next = $( $( this ).next() );
      if ( next.css( "max-height" ) === "0px" ) {
        icon.css( "transform", "rotate(-180deg)" );
        next.css( {
          "max-height": "400px",
          "padding": "10px",
          "opacity": "1"
        } );
      } else {
        icon.css( "transform", "rotate(0deg)" );
        next.css( {
          "max-height": "0",
          "opacity": "0",
          "padding": "0 10px"
        } );
      }
    } );


  } );
} )( $ );
